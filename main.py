from Utils.data_utils import get_academia_data_list
from Enums.enums import AcademiaFields
import time


def ping():
    start = time.time()
    get_academia_data_list(field=AcademiaFields.FINANCE, page_from_index=1, page_to_index=4)
    end = time.time()
    print('----------Elapsed Time:', int(end - start) / 60, ' minutes for 4 pages----------')


def get_cs_data():
    get_academia_data_list(field=AcademiaFields.CS, page_from_index=1, page_to_index=1000)


def get_finance_data():
    get_academia_data_list(field=AcademiaFields.FINANCE, page_from_index=1, page_to_index=1000)


def get_physics_data():
    get_academia_data_list(field=AcademiaFields.PHYSICS, page_from_index=1, page_to_index=1000)


def get_biology_data():
    get_academia_data_list(field=AcademiaFields.BIOLOGY, page_from_index=1, page_to_index=1000)


def main():
    ping()


if __name__ == "__main__":
    main()
