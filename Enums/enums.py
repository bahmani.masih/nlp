import enum


class AcademiaFields(enum.Enum):
    CS = 0,
    FINANCE = 1,
    PHYSICS = 2,
    BIOLOGY = 3,


academia_fields = {
    AcademiaFields.CS.value[0]: 'Computer_Science',
    AcademiaFields.FINANCE.value[0]: 'Finance',
    AcademiaFields.PHYSICS.value[0]: 'Physics',
    AcademiaFields.BIOLOGY.value[0]: 'Biology'
}


class Websites(enum.Enum):
    ACADEMIA = 0,


website_urls = {
    Websites.ACADEMIA.value[0]: 'https://www.academia.edu/Documents/in/'
}
