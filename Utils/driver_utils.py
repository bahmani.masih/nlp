from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from Enums.enums import *
import os


def get_website_url(website, field, page_index):
    url = website_urls[website.value[0]] + academia_fields[field.value[0]] + '?page=' + str(page_index)

    return url


def get_page_driver(website, field, page_index, open_browser=False):
    cwd = os.getcwd()

    website = get_website_url(website, field, page_index)
    driver_path = cwd + '/chromedriver'  # write the path here

    driver_options = Options()
    if not open_browser:
        driver_options = Options()
        driver_options.add_argument("headless")

    driver_options.add_experimental_option('excludeSwitches', ['enable-logging'])
    driver = webdriver.Chrome(
        service=Service(driver_path),
        options=driver_options
    )
    driver.get(website)

    print('Got page', field, page_index)
    return driver
