from selenium.webdriver.common.by import By

from Utils.driver_utils import get_page_driver
from Enums.enums import AcademiaFields, Websites
from Utils.file_utils import save_list_in_csv


def get_academia_data_list(field, page_from_index, page_to_index, save_in_file=True):
    print('Getting', field, 'data from Academia')
    counter = 0
    data = []

    for i in range(page_from_index, page_to_index+1):
        driver = get_page_driver(website=Websites.ACADEMIA, field=field, page_index=i)

        works = driver.find_element(by=By.CLASS_NAME, value='works').find_elements(by=By.XPATH, value="./*")
        for work in works:
            try:
                title = work.find_element(by=By.CLASS_NAME, value='header').text
            except Exception as e:
                print('Something happened while getting the title. Error message:')
                print(str(e))
                print('Trying to get the title again...')
                title = work.find_element(by=By.CLASS_NAME, value='header').text

            try:
                work.find_elements(by=By.TAG_NAME, value='a')[1].click()
                summary = work.find_element(by=By.CLASS_NAME, value='complete').text
            except Exception as e:
                summary = '-'

            data.append([
                counter, title, summary,
                1 if field == AcademiaFields.CS else 0,
                1 if field == AcademiaFields.FINANCE else 0,
                1 if field == AcademiaFields.PHYSICS else 0,
                1 if field == AcademiaFields.BIOLOGY else 0,
            ])
            counter += 1

        driver.quit()
        print('Page', i, 'added to data list')

        # HAS BE TO BE A MULTIPLE OF 20 FOR ACADEMIA (MULTIPLE OF PAGE SIZE)
        if save_in_file and counter % 40 == 0:
            save_list_in_csv(data)
            print('**** Data checkpoint at:', counter, '****')
            data.clear()
