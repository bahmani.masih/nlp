import csv


default_header_value = [['ID', 'title', 'abstract', 'Computer Science', 'Finance', 'Physics', 'Biology']]


def save_list_in_csv(data):
    should_add_header = False

    try:
        with open('data.csv', 'r', encoding='UTF8', newline='') as f:
            pass
    except FileNotFoundError as e:
        should_add_header = True
    finally:
        with open('data.csv', 'a', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)

            if should_add_header:
                writer.writerows(default_header_value)

            writer.writerows(data)

            f.close()
